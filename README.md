☀️ Qu’est-ce qui est affiché ? Déterminez votre adresse IP locale.

   Adresse IPv4. . . . . . . . . . . . . .: 10.33.71.44
   
☀️ Déterminez l’adresse réseau, l’adresse de diffusion (broadcast), le masque de sous-réseau, et votre adresse loopback

**Adresse IPv4 :** 10.33.71.44
**Masque de sous-réseau :** 255.255.240.0
**loopback :** Adresse IPv6 de liaison locale. . . . .: fe80::762e:646a:a6c1:5398%4(préféré)
**adress broadcast :** 192.168.23.255

☀️ Quelle est la passerelle du réseau Ynov ?

**Passerelle par défaut:** 10.33.79.254

☀️ Quelle est l’adresse IP que vous avez obtenue ?

**adresse :** 142.250.75.238

☀️ Notez les résultats

**8.8.8.8** : 8.8.8.8
**1.1.1.1** : 1.1.1.1

🔆 Qu’est-ce qui est affiché ? Essayez de déterminer quelles sont les adresses IP de votre machine, et quelle est l’adresse de loopback. Notez bien l’adresse IP de votre ordinateur.

**loopback :** 127.0.0.1/8
**broadcast :** 192.168.106.133



asresse ip client 1 : 192.168.106.134
asresse ip client 2 : 192.168.106.135

🔆 Depuis votre hôte, pingez vos VM une par une et notez le résultat.


**C:\Users\Nocthis>ping 192.168.106.135**

Envoi d’une requête 'Ping'  192.168.106.135 avec 32 octets de données :
Réponse de 192.168.106.135 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.135 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.135 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.135 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.106.135:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms

**C:\Users\Nocthis>ping 192.168.106.134**

Envoi d’une requête 'Ping'  192.168.106.134 avec 32 octets de données :
Réponse de 192.168.106.134 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.134 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.134 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.134 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.106.134:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms

**C:\Users\Nocthis>ping 192.168.106.133**

Envoi d’une requête 'Ping'  192.168.106.133 avec 32 octets de données :
Réponse de 192.168.106.133 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.133 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.133 : octets=32 temps<1ms TTL=64
Réponse de 192.168.106.133 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.106.133:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms


🔆 Depuis vos machines, utilisez nslookup pour trouver l’adresse IP de www.ynov.com et pingez cette adresse IP.
